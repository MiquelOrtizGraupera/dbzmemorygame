package com.example.memorygame.view
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import com.example.memorygame.R
import com.example.memorygame.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding:ActivityMainBinding
    lateinit var spinner:Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        spinner = binding.dificultySpinner

        spinner.adapter = ArrayAdapter.createFromResource(
            this,
            R.array.types,
            android.R.layout.simple_spinner_item
        )

        binding.playbutton.setOnClickListener {
            if(spinner.selectedItemPosition == 0){
                val intent = Intent(this, GameActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this, GameHardActivity::class.java)
                startActivity(intent)
            }
        }
        binding.helpbutton.setOnClickListener {
            mostrarDialog()
        }
    }

    fun mostrarDialog(){
        val builder = AlertDialog.Builder(this)
        builder.setMessage(" Easy: Hay 6 cartas, " +
                "haz match en pares Hard: Hay 8 cartas, haz match en pares")
        builder.show()
    }
}




