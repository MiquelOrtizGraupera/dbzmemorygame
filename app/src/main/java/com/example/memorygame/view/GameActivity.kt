package com.example.memorygame.view

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.memorygame.R
import com.example.memorygame.databinding.ActivityGameBinding
import com.example.memorygame.model.Card

class GameActivity : AppCompatActivity() {
    private lateinit var binding:ActivityGameBinding
    private var cardList = mutableListOf<Card>()
    private var turnedCardList = mutableListOf<Card>()
    private var counter: Int = 0
    private var counterPears: Int = 0

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        repartirCartes()

        binding.chronometer.start()


        binding.card1.setOnClickListener {
            binding.card1.setImageResource(girarCarta(0) )
            if(turnedCardList.size == 2){
                android.os.Handler().postDelayed({
                    comprovarCartes()
                }, 500)
            }
        }
        binding.card2.setOnClickListener {
            binding.card2.setImageResource(girarCarta(1) )
            if(turnedCardList.size == 2){
                android.os.Handler().postDelayed({
                    comprovarCartes()
                }, 500)
            }
        }
        binding.card3.setOnClickListener {
            binding.card3.setImageResource(girarCarta(2) )
            if(turnedCardList.size == 2){
                android.os.Handler().postDelayed({
                    comprovarCartes()
                }, 500)
            }
        }
        binding.card4.setOnClickListener {
            binding.card4.setImageResource(girarCarta(3) )
            if(turnedCardList.size == 2){
                android.os.Handler().postDelayed({
                    comprovarCartes()
                }, 500)
            }
        }
        binding.card5.setOnClickListener {
            binding.card5.setImageResource(girarCarta(4) )
            if(turnedCardList.size == 2){
                android.os.Handler().postDelayed({
                    comprovarCartes()
                }, 500)
            }
        }
        binding.card6.setOnClickListener {
            binding.card6.setImageResource(girarCarta(5) )
            if(turnedCardList.size == 2){
                android.os.Handler().postDelayed({
                    comprovarCartes()
                }, 1000)
            }
        }
    }

    private fun repartirCartes(){
        val picturesList = mutableListOf(
            R.drawable.gohan, R.drawable.goku3, R.drawable.vegeta,
            R.drawable.gohan, R.drawable.goku3, R.drawable.vegeta
        )

         picturesList.shuffle()

        for (i in 0..5){
            cardList.add(Card(i, picturesList[i],  turned = false, wined = false))
        }
    }

    private fun girarCarta(id:Int):Int{
        return if(cardList[id].wined){
            cardList[id].imageView
        } else if(!cardList[id].turned){
            cardList[id].turned = true
            turnedCardList.add(cardList[id])
            counter++
            cardList[id].imageView
        } else{
            cardList[id].turned = false
            R.drawable.backcard
        }
    }

    private fun comprovarCartes(){
        if(turnedCardList.size == 2 && turnedCardList[0].imageView == turnedCardList[1].imageView){
            for (card in cardList){
                if (card.id == turnedCardList[0].id || card.id == turnedCardList[1].id)
                    cardList[card.id].wined = true
            }
            turnedCardList.clear()
            counterPears++
            if(counterPears == 3){
                endGame()
            }
        }
        else{
            for (card in cardList){
                if(card.id == turnedCardList[0].id || card.id == turnedCardList[1].id) {
                    card.turned = false
                }
            }
            resetUI()
        }
        turnedCardList.clear()

    }

    private fun resetUI(){
       for(card  in cardList){
           if(!card.turned){
               when(card.id){
                   0 -> binding.card1.setImageResource(R.drawable.backcard)
                   1 -> binding.card2.setImageResource(R.drawable.backcard)
                   2 -> binding.card3.setImageResource(R.drawable.backcard)
                   3 -> binding.card4.setImageResource(R.drawable.backcard)
                   4 -> binding.card5.setImageResource(R.drawable.backcard)
                   5 -> binding.card6.setImageResource(R.drawable.backcard)
               }
           }
       }
    }

    private fun endGame(){
        if(counterPears == 3){
            val intent = Intent(this, ScoreActivity::class.java)
            intent.putExtra("puntuacio", puntuacio())
            startActivity(intent)
        }
    }

    private fun puntuacio():Int {
        return 100 - (counter -6)*10
    }


}