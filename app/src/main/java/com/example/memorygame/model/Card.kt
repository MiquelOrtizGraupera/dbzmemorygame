package com.example.memorygame.model

data class Card(
    val id:Int,
    var imageView: Int,
    var turned:Boolean = false,
    var wined:Boolean = false
)
